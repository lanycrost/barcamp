import App from './app/reducer';
import LanguageSwitcher from './languageSwitcher/reducer';

export default {
  App,
  LanguageSwitcher
};
